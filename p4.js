
$.fn.puissance = function()
{
	// console.log("cacahuète");
	class puissance4
	{
		constructor(selector)
		{
			this.COL = 7;
			this.LGN = 6;
			this.selector = selector;
			this.player = 'player1';
			this.score = 0;

			this.grid();
			this.game();
			this.checkWin();
		}

		grid()
		{
			const $jeu = $(this.selector);
			$(this.selector).text('');
			for(let lgn = 0; lgn < this.LGN; lgn++)
			{
				const $lgn = $('<div>').addClass('lgn');

				for(let col= 0; col < this.COL; col++)
				{
					const $col = $('<div>').addClass('col empty').attr("data-col", col).attr("data-lgn", lgn);
					$lgn.append($col);
				}

				$jeu.append($lgn);

			}
		}


		game()
		{
			const $jeu = $(this.selector);
			const that = this;

			function lastCase(col)
			{
				const $cells = $(`.col[data-col='${col}']`);
				for(let i = $cells.length-1; i>= 0; i--)
				{
					const $cell = $($cells[i]);
					if($cell.hasClass('empty'))
					{
						return $cell;
					}
				}
				return null;
			}

			$jeu.on('click', '.col.empty', function()
			{
				const col = $(this).data('col');
				const $last =  lastCase(col);
				$last.addClass(`${that.player}`).removeClass('empty').data('player', `${that.player}`);

				const winner = that.checkWin($last.data('lgn'), $last.data('col'));
				that.player = (that.player === 'player1') ? 'player2' : 'player1';

				var score = 0;
				if(winner == 'player1')
				{
					score++;
					$("#scoreP1").text(score);
				}
				
				if(winner == 'player2')
				{
					score++;
					$("#scoreP2").text(score);
				}


				if(winner)
				{
					alert(`${winner} a gagné la partie`);
					return;
				}
				console.log(winner);


				$("#show_player").text(that.player);
			});

			$('#restart').on('click', function()
			{
				that.grid();
			});
		}




		checkWin(lgn, col)
		{
			const that = this;

			function $getCell(x, y)
			{
				return $(`.col[data-lgn='${x}'][data-col='${y}']`);
			}

			function checkDirection(direction)
			{
				let total = 0;
				let x = lgn + direction.x;
				let y = col + direction.y;
				let $next = $getCell(x, y);

				while(x >= 0 && x < that.LGN && y >= 0 && y < that.COL && $next.data('player') === that.player)
				{
					total++;
					x += direction.x;
					y += direction.y;
					$next = $getCell(x, y);
				}
				return total;
			}

			function checkWin(directionX, directionY)
			{
				const total = 1 + checkDirection(directionX) + checkDirection(directionY);
				if(total >= 4)
				{
					return that.player;
				}
				else
				{
					return null;
				}
			}

			function hori()
			{
				return checkWin({x: 0, y: -1}, {x: 0, y: 1});
			}

			function verti()
			{
				return checkWin({x: -1, y: 0}, {x: 1, y: 0});
			}

			function diag1()
			{

				return checkWin({x: 1, y: 1}, {x: -1, y: -1});
			}

			function diag2()
			{
				return checkWin({x: 1, y: -1}, {x: -1, y: 1});
			}

			return hori() || verti() || diag1() || diag2();
		}
	}
	const puiss4 = new puissance4('#game');
};

$('document').ready(function()
{
	$('document').puissance();
});


